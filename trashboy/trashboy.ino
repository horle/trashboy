#include <WiFi.h>
#include <PubSubClient.h>
#include <HTTPClient.h>

const char* ssid = "SSID";
const char* pass = "PASS";

const uint16_t port = 1883;
const char* host = "IP";

const int GELB_LED = 5;
const int REST_LED = 17;
const int BIO_LED = 18;
const int PAPIER_LED = 19;

WiFiClient wifi;
PubSubClient client(wifi);
HTTPClient http;

void setup() {
   // debug
   Serial.begin(115200);
   while (!Serial);

   pinMode(GELB_LED, OUTPUT);
   pinMode(REST_LED, OUTPUT);
   pinMode(BIO_LED, OUTPUT);
   pinMode(PAPIER_LED, OUTPUT);

   client.setServer(host, port);
   client.setCallback(callback);
}

void connectWiFi() {
   if (WiFi.status() == WL_CONNECTED) return;
   WiFi.begin(ssid, pass);

   while (WiFi.status() != WL_CONNECTED) {
      delay(500);
   }

   Serial.print("connected! IP: ");
   Serial.println(WiFi.localIP());
}

void connectHost() {   
   // Loop until we're reconnected
   while (!client.connected()) {
      Serial.print("Attempting MQTT connection...");
      String clientId = "trashboy-1";
      
      if (client.connect(clientId.c_str())) {
         Serial.println("connected");
         client.subscribe("trash");
      }
      else {
         Serial.print("failed, rc=");
         Serial.print(client.state());
         Serial.println(" trying again in 5 seconds");
         // Wait 5 seconds before retrying
         delay(5000);
      }
   }
}

void callback(char* topic, byte* payload, unsigned int length) {

   if (length != 4) {
      Serial.println("message from broker malformed");
      return;
   }
   
   for (int i = 0; i < length; i++) {
      Serial.print((char)payload[i]);
   }
   Serial.println();

   if ((char)payload[0] == '1') {
      Serial.println("gelber sack");
      digitalWrite(GELB_LED, HIGH);
   } else {
      digitalWrite(GELB_LED, LOW);
   }
   if ((char)payload[1] == '1') {
      Serial.println("restmuell");
      digitalWrite(REST_LED, HIGH);
   } else {
      digitalWrite(REST_LED, LOW);
   }
   if ((char)payload[2] == '1') {
      Serial.println("bio");
      digitalWrite(BIO_LED, HIGH);
   } else {
      digitalWrite(BIO_LED, LOW);
   }
   if ((char)payload[3] == '1') {
      Serial.println("papier");
      digitalWrite(PAPIER_LED, HIGH);
   } else {
      digitalWrite(PAPIER_LED, LOW);
   }
}

void loop() {
   // wait for network connection
   connectWiFi();
   // wait for host connection
   connectHost();

   client.loop();

   delay(2000);
}
