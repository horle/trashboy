#include <WiFi.h>
#include <HTTPClient.h>
#include <SPIFFS.h>

const char* ssid = "SSID";
const char* pass = "PASS";

const uint16_t port = 1883;
const char* host = "IP";

const int GELB_LED = 5;
const int REST_LED = 17;
const int BIO_LED = 18;
const int PAPIER_LED = 19;

WiFiClient wifi;

void setup() {
   // debug
   Serial.begin(115200);
   while (!Serial);

   pinMode(GELB_LED, OUTPUT);
   pinMode(REST_LED, OUTPUT);
   pinMode(BIO_LED, OUTPUT);
   pinMode(PAPIER_LED, OUTPUT);

   SPIFFS.begin();
}

void connectWiFi() {
   if (WiFi.status() == WL_CONNECTED) return;
   WiFi.begin(ssid, pass);
   WiFi.setSleep(false);

   while (WiFi.status() != WL_CONNECTED) {
      delay(500);
   }

   Serial.print("connected! IP: ");
   Serial.println(WiFi.localIP());
}

void connectHost() {   
   
}

int getTrashInfo() {

   while (WiFi.status() != WL_CONNECTED) {
      Serial.print("waiting for network: core ");
      Serial.println(xPortGetCoreID());
      delay(1000);
   }
   
   if (WiFi.status() == WL_CONNECTED) {

      String filename = "/trash";

      /*SPIFFS.remove(filename);
      File f = SPIFFS.open(filename, "w+");
      
      HTTPClient http;
      //http.begin("https://www.siegen.de/leben-in-siegen/buergerservice/abfallentsorgung/abfallkalender/?tx_abfallkalender_abfall%5Buid%5D=3925&tx_abfallkalender_abfall%5Byear%5D=2019&tx_abfallkalender_abfall%5Baction%5D=download&tx_abfallkalender_abfall%5Bcontroller%5D=Events&cHash=e705d87cd8a94f49f0e0abc49b998926");
      http.begin("https://www.siegen.de/leben-in-siegen/buergerservice/abfallentsorgung/abfallkalender/?tx_abfallkalender_abfall%5Buid%5D=3925&tx_abfallkalender_abfall%5Baction%5D=listplan&tx_abfallkalender_abfall%5Bcontroller%5D=Events&cHash=2a3337f57ed3df5e47e543fcaa0a236a");

      //malloc(200000);
      Serial.println(ESP.getFreeHeap());

      int code = http.GET();
      http.writeToStream(&f);
      int ret = 0;
      Serial.println(String("http code ") + String(code));

      f.close();*/

      int code = 1;

      // https://www.siegen.de/leben-in-siegen/buergerservice/abfallentsorgung/abfallkalender/
      // ?tx_abfallkalender_abfall%5Buid%5D=3925             fix
      // &tx_abfallkalender_abfall%5Byear%5D=2019            year
      // &tx_abfallkalender_abfall%5Baction%5D=listplan      fix
      // &tx_abfallkalender_abfall%5Bcontroller%5D=Events    fix
      // &cHash=e705d87cd8a94f49f0e0abc49b998926             ??
      
      if (code > 0) {
         Serial.println(ESP.getFreeHeap());
         File f = SPIFFS.open(filename, "r");
         while (f.available())
         {
            Serial.write(f.read());
         }
         f.close();
      }
      else {
         Serial.println(String("error http: ") + String(code));
      }
      //http.end();
      WiFi.setSleep(true);
   
      //return ret;
      return 0;
   }
}

void updateLEDs(int val) {

   bool gelb = (val >> 3) & 1;
   bool rest = (val >> 2) & 1;
   bool bio = (val >> 1) & 1;
   bool papier = val & 1;
   
   digitalWrite(GELB_LED, gelb);
   digitalWrite(REST_LED, rest);
   digitalWrite(BIO_LED, bio);
   digitalWrite(PAPIER_LED, papier);
}

void loop() {
   // wait for network connection
   connectWiFi();
   // wait for host connection
   connectHost();

   updateLEDs(getTrashInfo());

   delay(20000000);
}
